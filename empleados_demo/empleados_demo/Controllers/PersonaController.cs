﻿using empleados_demo.Entities;
using empleados_demo.Models.Operation;
using empleados_demo.Models.Operation.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace empleados_demo.Controllers
{
    public class PersonaController : Controller
    {
        private DatabaseContext dbContext = new DatabaseContext();      
        
        // GET: Persona
        public ActionResult Index()
        {
            var persons = dbContext.Personas;

            var result= (from per in persons
                    select new PersonaDTO
                    {
                        Id = per.Id,
                        Nombre = per.Nombre,
                        ApellidoP = per.ApellidoP,
                        FechaNacimiento = per.FechaNacimiento
                    }).ToList();

            return View(result);
        }
    }
}