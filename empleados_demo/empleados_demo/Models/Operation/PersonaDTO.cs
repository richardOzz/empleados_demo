﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace empleados_demo.Models.Operation
{
    public class PersonaDTO
    {
        public int Id { get; set; }

        public string Nombre { get; set; }
        public string ApellidoP { get; set; }

        public DateTime FechaNacimiento { get; set; }
    }
}