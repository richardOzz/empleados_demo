﻿using empleados_demo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace empleados_demo.Models.Operation.DAO
{
    public class OperationDAO 
    {
        private DatabaseContext dbContext;
        
     
        public OperationDAO(DatabaseContext dbCnt)
        {
            dbContext = dbCnt;
        }

        public List<PersonaDTO> GetPersonas()
        {
           var persons= dbContext.Personas;

            return (from per in persons
                    select new PersonaDTO
                    {
                        Id = per.Id,
                        Nombre = per.Nombre,
                        ApellidoP = per.ApellidoP,
                        FechaNacimiento = per.FechaNacimiento
                    }).ToList();
        }

        public List<PersonaDTO> GetPersonasDummy()
        {
            //TODO llamar a dAO con conexion a sqlite
            return GetDummyData();
        }


        public List<PersonaDTO>  GetDummyData()
        {
            return new List<PersonaDTO>
            {
                new PersonaDTO
                {
                    Id = 1,
                    Nombre = "Sony",
                    ApellidoP = "Xina",
                    FechaNacimiento=new DateTime(1990,5,1)
                },
               new PersonaDTO
                {
                    Id = 2,
                    Nombre = "Australia",
                    ApellidoP = "Kangaroo",
                    FechaNacimiento =new DateTime(1990,5,1)
                }
            };
        }
    }
}