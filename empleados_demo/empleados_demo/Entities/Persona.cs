﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace empleados_demo.Entities
{
    [Table("Persona")]
    public class Persona
    {
        [Key]
        public int Id { get; set; }

        public string Nombre { get; set; }
        public string ApellidoP { get; set; }

        public DateTime FechaNacimiento { get; set; }

    }
}