﻿
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;


namespace empleados_demo.Entities
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext():base("mydbSsz")
        {
            //Database.SetInitializer<DatabaseContext>(new CreateDatabaseIfNotExists<DatabaseContext>());
        }

        public DbSet<Persona> Personas { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

    }
}